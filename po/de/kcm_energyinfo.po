# Burkhard Lück <lueck@hube-lueck.de>, 2015, 2016, 2018, 2019.
# Frederik Schwarzer <schwarzer@kde.org>, 2015, 2020, 2022, 2023.
# SPDX-FileCopyrightText: 2023 Johannes Obermayr <johannesobermayr@gmx.de>
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-14 01:36+0000\n"
"PO-Revision-Date: 2023-12-20 22:50+0100\n"
"Last-Translator: Johannes Obermayr <johannesobermayr@gmx.de>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.4\n"

#: ui/main.qml:45
#, kde-format
msgid "Battery"
msgstr "Akku"

#: ui/main.qml:47
#, kde-format
msgid "Rechargeable"
msgstr "Wiederaufladbar"

#: ui/main.qml:48
#, kde-format
msgid "Charge state"
msgstr "Ladestatus"

#: ui/main.qml:49
#, kde-format
msgid "Current charge"
msgstr "Aktueller Ladezustand"

#: ui/main.qml:50
#, kde-format
msgid "Health"
msgstr "Alterungszustand"

#: ui/main.qml:51
#, kde-format
msgid "Vendor"
msgstr "Lieferant"

#: ui/main.qml:52
#, kde-format
msgid "Model"
msgstr "Modell"

#: ui/main.qml:53
#, kde-format
msgid "Serial Number"
msgstr "Seriennummer"

#: ui/main.qml:54
#, kde-format
msgid "Technology"
msgstr "Technologie"

#: ui/main.qml:58
#, kde-format
msgid "Energy"
msgstr "Energie"

#: ui/main.qml:60
#, kde-format
msgctxt "current power draw from the battery in W"
msgid "Consumption"
msgstr "Verbrauch"

#: ui/main.qml:60
#, kde-format
msgctxt "Watt"
msgid "W"
msgstr "W"

#: ui/main.qml:61
#, kde-format
msgid "Voltage"
msgstr "Spannung"

#: ui/main.qml:61
#, kde-format
msgctxt "Volt"
msgid "V"
msgstr "V"

#: ui/main.qml:62
#, kde-format
msgid "Remaining energy"
msgstr "Verbleibende Ladung"

#: ui/main.qml:62 ui/main.qml:63 ui/main.qml:64
#, kde-format
msgctxt "Watt-hours"
msgid "Wh"
msgstr "Wh"

#: ui/main.qml:63
#, kde-format
msgid "Last full charge"
msgstr "Zuletzt vollständig geladen"

#: ui/main.qml:64
#, kde-format
msgid "Original charge capacity"
msgstr "Ursprüngliche Ladungskapazität"

#: ui/main.qml:68
#, kde-format
msgid "Environment"
msgstr "Umgebung"

#: ui/main.qml:70
#, kde-format
msgid "Temperature"
msgstr "Temperatur"

#: ui/main.qml:70
#, kde-format
msgctxt "Degree Celsius"
msgid "°C"
msgstr "°C"

#: ui/main.qml:77
#, kde-format
msgid "Not charging"
msgstr "Wird nicht geladen"

#: ui/main.qml:78
#, kde-format
msgid "Charging"
msgstr "Wird geladen"

#: ui/main.qml:79
#, kde-format
msgid "Discharging"
msgstr "Wird entladen"

#: ui/main.qml:80
#, kde-format
msgid "Fully charged"
msgstr "Vollständig geladen"

#: ui/main.qml:86
#, kde-format
msgid "Lithium ion"
msgstr "Lithium-Ionen"

#: ui/main.qml:87
#, kde-format
msgid "Lithium polymer"
msgstr "Lithium-Polymer"

#: ui/main.qml:88
#, kde-format
msgid "Lithium iron phosphate"
msgstr "Lithium-Eisen-Phosphat"

#: ui/main.qml:89
#, kde-format
msgid "Lead acid"
msgstr "Bleisäure"

#: ui/main.qml:90
#, kde-format
msgid "Nickel cadmium"
msgstr "Nickel-Cadmium"

#: ui/main.qml:91
#, kde-format
msgid "Nickel metal hydride"
msgstr "Nickel-Metall-Hybrid"

#: ui/main.qml:92
#, kde-format
msgid "Unknown technology"
msgstr "Unbekannte Technologie"

#: ui/main.qml:99
#, kde-format
msgid "Last hour"
msgstr "Letzte Stunde"

#: ui/main.qml:99
#, kde-format
msgid "Last 2 hours"
msgstr "Letzte 2 Stunden"

#: ui/main.qml:99
#, kde-format
msgid "Last 12 hours"
msgstr "Letzte 12 Stunden"

#: ui/main.qml:99
#, kde-format
msgid "Last 24 hours"
msgstr "Letzte 24 Stunden"

#: ui/main.qml:99
#, kde-format
msgid "Last 48 hours"
msgstr "Letzte 48 Stunden"

#: ui/main.qml:99
#, kde-format
msgid "Last 7 days"
msgstr "Letzte 7 Tage"

#: ui/main.qml:106
#, kde-format
msgctxt "@info:status"
msgid "No energy information available on this system"
msgstr ""
"Auf diesem System sind keine Informationen zum Energieverbrauch verfügbar"

#: ui/main.qml:182
#, kde-format
msgid "PDA Battery"
msgstr "PDA-Akku"

#: ui/main.qml:183
#, kde-format
msgid "UPS Battery"
msgstr "UPS-Akku"

#: ui/main.qml:184
#, kde-format
msgid "Internal Battery"
msgstr "Interner Akku"

#: ui/main.qml:185
#, kde-format
msgid "Mouse Battery"
msgstr "Maus-Akku"

#: ui/main.qml:186
#, kde-format
msgid "Keyboard Battery"
msgstr "Tastatur-Akku"

#: ui/main.qml:187
#, kde-format
msgid "Keyboard/Mouse Battery"
msgstr "Tastatur/Maus-Akku"

#: ui/main.qml:188
#, kde-format
msgid "Camera Battery"
msgstr "Kamera-Akku"

#: ui/main.qml:189
#, kde-format
msgid "Phone Battery"
msgstr "Telefon-Akku"

#: ui/main.qml:190
#, kde-format
msgid "Monitor Battery"
msgstr "Monitor-Akku"

#: ui/main.qml:191
#, kde-format
msgid "Gaming Input Battery"
msgstr "Spieleingabe-Akku"

#: ui/main.qml:192
#, kde-format
msgid "Bluetooth Battery"
msgstr "Bluetooth-Akku"

#: ui/main.qml:193
#, kde-format
msgid "Tablet Battery"
msgstr "Tablet-Akku"

#: ui/main.qml:194
#, kde-format
msgid "Headphone Battery"
msgstr "Kopfhörer-Akku"

#: ui/main.qml:195
#, kde-format
msgid "Headset Battery"
msgstr "Headset-Akku"

#: ui/main.qml:196
#, kde-format
msgid "Touchpad Battery"
msgstr "Touchpad-Akku"

#: ui/main.qml:197
#, kde-format
msgid "Unknown Battery"
msgstr "Unbekannter Akku"

#: ui/main.qml:225
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1% (Charging)"
msgstr "%1% (Wird geladen)"

#: ui/main.qml:226
#, kde-format
msgctxt "Battery charge percentage"
msgid "%1%"
msgstr "%1 %"

#: ui/main.qml:275
#, kde-format
msgctxt "Shorthand for Watts"
msgid "W"
msgstr "W"

#: ui/main.qml:275
#, kde-format
msgctxt "literal percent sign"
msgid "%"
msgstr " %"

#: ui/main.qml:301
#, kde-format
msgid "Charge Percentage"
msgstr "Ladung in Prozent"

#: ui/main.qml:311
#, kde-format
msgid "Energy Consumption"
msgstr "Energieverbrauch"

#: ui/main.qml:326
#, kde-format
msgid "Timespan"
msgstr "Zeitraum"

#: ui/main.qml:327
#, kde-format
msgid "Timespan of data to display"
msgstr "Zeitraum der anzuzeigenden Daten"

#: ui/main.qml:333
#, kde-format
msgid "Refresh"
msgstr "Aktualisieren"

#: ui/main.qml:345
#, kde-format
msgid "This type of history is currently not available for this device."
msgstr "Diese Art des Verlaufs ist zurzeit für dieses Gerät nicht verfügbar."

#: ui/main.qml:403
#, kde-format
msgid "%1:"
msgstr "%1:"

#: ui/main.qml:414
#, kde-format
msgid "Yes"
msgstr "Ja"

#: ui/main.qml:416
#, kde-format
msgid "No"
msgstr "Nein"

#: ui/main.qml:436
#, kde-format
msgctxt "%1 is a percentage value"
msgid "%1%"
msgstr "%1 %"

#: ui/main.qml:438
#, kde-format
msgctxt "%1 is value, %2 is unit"
msgid "%1 %2"
msgstr "%1 %2"

#~ msgctxt "Battery health percentage"
#~ msgid "%1%"
#~ msgstr "%1 %"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "This module lets you see energy information and statistics."
#~ msgstr ""
#~ "In diesem Modul werden Informationen und Statistiken zum Energieverbrauch "
#~ "angezeigt."

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Burkhard Lück"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "lueck@hube-lueck.de"

#~ msgid "Energy Consumption Statistics"
#~ msgstr "Statistiken über den Energieverbrauch"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"

#~ msgid "Application Energy Consumption"
#~ msgstr "Energieverbrauch von Programmen"

#~ msgid "Path: %1"
#~ msgstr "Pfad: %1"

#~ msgid "PID: %1"
#~ msgstr "PID: %1"

#~ msgid "Wakeups per second: %1 (%2%)"
#~ msgstr "Weckrufe pro Sekunde: %1 (%2 %)"

#~ msgid "Details: %1"
#~ msgstr "Details: %1"

#~ msgid "Capacity degradation"
#~ msgstr "Kapazitätsverminderung"

#~ msgid "Manufacturer"
#~ msgstr "Hersteller"

#~ msgid "Full design"
#~ msgstr "„Voll“-Design"

#~ msgid "System"
#~ msgstr "System"

#~ msgid "Has power supply"
#~ msgstr "Hat Stromversorgung"

#~ msgid "Capacity"
#~ msgstr "Kapazität"
