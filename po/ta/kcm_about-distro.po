# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kinfocenter package.
#
# Kishore G <kishore96@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: kinfocenter\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-31 00:38+0000\n"
"PO-Revision-Date: 2023-07-30 12:11+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: CPUEntry.cpp:17
#, kde-format
msgid "Processor:"
msgid_plural "Processors:"
msgstr[0] "கணிப்பி:"
msgstr[1] "கணிப்பிகள்:"

#: CPUEntry.cpp:22
#, kde-format
msgctxt ""
"unknown CPU type/product name; presented as `Processors: 4 × Unknown Type'"
msgid "Unknown Type"
msgstr "தெரியாத வகை"

#: GPUEntry.cpp:19
#, kde-format
msgid "Graphics Processor:"
msgstr "வரைகலை கணிப்பி:"

#: GraphicsPlatformEntry.cpp:10
#, kde-format
msgid "Graphics Platform:"
msgstr "வரைகலை இயக்குதளம்:"

#: KernelEntry.cpp:11
#, kde-format
msgid "Kernel Version:"
msgstr "கருனி பதிப்பு:"

#: KernelEntry.cpp:23
#, kde-format
msgctxt "@label %1 is the kernel version, %2 CPU bit width (e.g. 32 or 64)"
msgid "%1 (%2-bit)"
msgstr "%1 (%2-பிட்டு)"

#: main.cpp:197
#, kde-format
msgctxt "@label"
msgid "Manufacturer:"
msgstr "உற்பத்தியாளர்:"

#: main.cpp:200 main.cpp:217
#, kde-format
msgctxt "@label"
msgid "Product Name:"
msgstr "உற்பத்திப்பொருளின் பெயர்:"

#: main.cpp:203
#, kde-format
msgctxt "@label"
msgid "System Version:"
msgstr "கணினியின் வகை:"

#: main.cpp:206 main.cpp:220
#, kde-format
msgctxt "@label"
msgid "Serial Number:"
msgstr "தொடர் எண்:"

#: main.cpp:210 main.cpp:227
#, kde-format
msgctxt "@label unknown entry in table"
msgid "Unknown:"
msgstr "தெரியாதது:"

#: main.cpp:223
#, kde-format
msgctxt "@label uboot is the name of a bootloader for embedded devices"
msgid "U-Boot Version:"
msgstr "U-boot பதிப்பு:"

#: main.cpp:261
#, kde-format
msgid "KDE Frameworks Version:"
msgstr "கே.டீ.யீ. நிரலகங்களின் பதிப்பு:"

#: main.cpp:262
#, kde-format
msgid "Qt Version:"
msgstr "Qt பதிப்பு:"

#: MemoryEntry.cpp:20
#, kde-format
msgid "Memory:"
msgstr "நினைவகம்:"

#: MemoryEntry.cpp:49
#, kde-format
msgctxt "@label %1 is the formatted amount of system memory (e.g. 7,7 GiB)"
msgid "%1 of RAM"
msgstr "%1 RAM"

#: MemoryEntry.cpp:53
#, kde-format
msgctxt "Unknown amount of RAM"
msgid "Unknown"
msgstr "தெரியாதது"

#: OSVersionEntry.cpp:9
#, kde-format
msgid "Operating System:"
msgstr "இயக்குதளம்:"

#: OSVersionEntry.cpp:11
#, kde-format
msgctxt "@label %1 is the distro name, %2 is the version"
msgid "%1 %2"
msgstr "%1 %2"

#: OSVersionEntry.cpp:13
#, kde-format
msgctxt ""
"@label %1 is the distro name, %2 is the version, %3 is the 'build' which "
"should be a number, or 'rolling'"
msgid "%1 %2 Build: %3"
msgstr "%1 %2 பதிப்பு: %3"

#: PlasmaEntry.cpp:15
#, kde-format
msgid "KDE Plasma Version:"
msgstr "கே.டீ.யீ பிளாஸ்மா பதிப்பு:"

#: ThirdPartyEntry.cpp:12
#, kde-format
msgctxt "Unused but needs to be : to avoid assertion in Entry constructor"
msgid ":"
msgstr ":"

#: ui/main.qml:90
#, kde-format
msgctxt "@title"
msgid "Serial Number"
msgstr "தொடர் எண்"

#: ui/main.qml:96
#, kde-format
msgctxt "@action:button"
msgid "Copy"
msgstr "நகலெடு"

#: ui/main.qml:123
#, kde-format
msgctxt "@action:button show a hidden entry in an overlay"
msgid "Show"
msgstr "காட்டு"

#: ui/main.qml:135
#, kde-format
msgctxt "@title:group"
msgid "Software"
msgstr "மென்பொருள்"

#: ui/main.qml:145
#, kde-format
msgctxt "@title:group"
msgid "Hardware"
msgstr "வன்பொருள்"

#: ui/main.qml:165
#, kde-format
msgctxt "@action:button launches kinfocenter from systemsettings"
msgid "Launch %1"
msgstr "%1 தனை திற"

#: ui/main.qml:178 ui/main.qml:186
#, kde-format
msgctxt "@action:button"
msgid "Copy Details"
msgstr "விவரங்களை நகலெடு"

#: ui/main.qml:189
#, kde-format
msgctxt "@action:button Copy Details..."
msgid "In current language"
msgstr "தற்போதைய மொழியில்"

#: ui/main.qml:195
#, kde-format
msgctxt "@action:button Copy Details..."
msgid "In English"
msgstr "ஆங்கிலத்தில்"

#~ msgctxt "@action:button"
#~ msgid "Copy to Clipboard"
#~ msgstr "பிடிப்புப்பலகைக்கு நகலெடு"

#~ msgctxt "@action:button"
#~ msgid "Copy to Clipboard in English"
#~ msgstr "பிடிப்புப்பலகைக்கு ஆங்கிலத்தில் நகலெடு"

#~ msgctxt "@action:button launches kinfocenter from systemsettings"
#~ msgid "Show More Information"
#~ msgstr "மேலும் விவரங்களை காட்டு"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "கோ. கிஷோர்"

#~ msgctxt "@title"
#~ msgid "About this System"
#~ msgstr "இக்கணினியை பற்றி"

#~ msgctxt "@info:credit"
#~ msgid "Copyright 2012-2020 Harald Sitter"
#~ msgstr "பதிப்புரிமை 2012–2020 ஹாரல்ட் சிட்டர்"

#~ msgctxt "@info:credit"
#~ msgid "Harald Sitter"
#~ msgstr "ஹாரல்ட் சிட்டர்"

#~ msgctxt "@info:credit"
#~ msgid "Author"
#~ msgstr "இயற்றியவர்"
